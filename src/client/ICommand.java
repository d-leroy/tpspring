package client;

import client.impl.log.ChatUserCtl;

public interface ICommand {

	public void execute();
	public void setCtl(ChatUserCtl ctl);
}
