package client.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginException;

import server.IChatRoom;
import client.IChatUser;
import client.IUser;
import client.SampleLoginModule;
import client.impl.log.ChatUI;

import com.sun.security.auth.callback.DialogCallbackHandler;


public class ChatUser extends UnicastRemoteObject implements IChatUser, IUser, Runnable {
	
	
	private IChatRoom room=null;
	private String pseudo=null;
	private ChatUI ui=null;

	public ChatUser() throws RemoteException {
		super();
	}

	public void displayMessage(String message) throws RemoteException {
		ui.displayMessage(message);
	}

	public void run() {
		try {
			SampleLoginModule lc = null;
			try {
				lc = new SampleLoginModule(room);
				//Set principals = new HashSet();
				//principals
				//		.add(new sample.principal.SamplePrincipal("testUser"));
				//Subject mySubject = new Subject(false, principals,
				//		new HashSet(), new HashSet());

				lc.initialize(new Subject(), new DialogCallbackHandler(), null,
						new HashMap());

			} catch (SecurityException se) {
				System.err.println("Cannot create LoginContext. "
						+ se.getMessage());
				System.exit(-1);
			}

			// the user has 3 attempts to authenticate successfully
			int i;
			for (i = 0; i < 3; i++) {
				try {

					// attempt authentication
					lc.login();

					// if we return with no exception, authentication succeeded
					break;

				} catch (LoginException le) {

					System.err.println("Authentication failed:");
					System.err.println("  " + le.getMessage());
					try {
						Thread.currentThread().sleep(3000);
					} catch (Exception e) {
						// ignore
					}

				}
			}

			// did they fail three times?
			if (i == 3) {
				System.out.println("Sorry");
				
				System.exit(-1);
			}

			System.out.println("Authentication succeeded!");
			this.pseudo = ui.requestPseudo();
		    if (pseudo == null) System.exit(0);
			this.room.subscribe(this, this.pseudo);

		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public String getPseudo() {
		return pseudo;
	}
	
	public void setChatUI(ChatUI ui) {
		this.ui = ui;
	}

	public void setChatRoom(IChatRoom chatRoom) {
		this.room = chatRoom;
	}

}