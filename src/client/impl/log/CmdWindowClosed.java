package client.impl.log;

import client.ICommand;


public class CmdWindowClosed implements ICommand {

	ChatUserCtl ctl = null;
	
	public void execute() {
		ctl.handleWindowClosed();
	}

	public void setCtl(ChatUserCtl ctl) {
		this.ctl = ctl;
		
	}

}
