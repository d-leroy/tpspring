package client.impl.log;

import client.ICommand;



public class CmdBtnSendClicked implements ICommand {

	ChatUserCtl ctl = null;

	public void execute() {
		ctl.handleBtnSendClicked();
	}

	public void setCtl(ChatUserCtl ctl) {
		this.ctl = ctl;
		
	}

}
