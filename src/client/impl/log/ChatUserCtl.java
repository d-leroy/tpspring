package client.impl.log;

import java.rmi.Naming;
import java.rmi.RemoteException;

import client.impl.ChatUser;
import server.IChatRoom;


public class ChatUserCtl {

	private static final String uri = "rmi://localhost/ChatRoom";
	
	private ChatUI chatUI = null;
	private IChatRoom chatRoom = null;
	private ChatUser chatUser = null;
	
	public ChatUserCtl() {
		try {
			this.chatRoom = (IChatRoom) Naming.lookup(uri);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public void handleBtnSendClicked() {
		try {
			chatRoom.postMessage(chatUser.getPseudo(), chatUI.getTxtMessage());
		} catch (RemoteException e) {
			e.printStackTrace();
		} finally {
			chatUI.clearText();
		}
	}
	
	public void handleWindowClosed(){
		try {
			chatRoom.unsubscribe(chatUser.getPseudo());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void setChatUI(ChatUI chatUI) {
		this.chatUI = chatUI;
	}

	public void setChatUser(ChatUser chatUser) {
		this.chatUser = chatUser;
		this.chatUser.setChatRoom(this.chatRoom);
	}
}
