package client;

import java.rmi.RemoteException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import client.impl.ChatUser;

public class Main {
	public static void main(String[] args) throws RemoteException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"client-beans.xml"});
		ChatUser usr = (ChatUser) context.getBean("beanChatUser");
		
		new Thread(usr).start();
		
	}

}
